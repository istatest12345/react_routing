import React from 'react';

function Index() {
	return (
		<form className="container p-5 m-5 row g-3 needs-validation" novalidate>
			<div className="col-md-4 position-relative">
				<label for="validationTooltip01" className="form-label">First name</label>
				<input type="text" className="form-control" id="validationTooltip01" required />
				<div className="valid-tooltip">
					Looks good!
				</div>
			</div>
			<div className="col-md-4 position-relative">
				<label for="validationTooltip02" className="form-label">Last name</label>
				<input type="text" className="form-control" id="validationTooltip02" required />
				<div className="valid-tooltip">
					Looks good!
				</div>
			</div>
			<div className="col-md-6 position-relative">
				<label for="validationTooltip03" className="form-label">City</label>
				<textarea rows="6" className="form-control" id="validationTooltip03" required> </textarea>
				<div className="invalid-tooltip">
					Please provide a valid city.
				</div>
			</div>

			<div className="col-12">
				<button className="btn btn-primary" type="submit">Submit form</button>
			</div>
		</form>
	);
}

export default Index;