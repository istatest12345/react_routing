import React from 'react';
import data from "../../companies.json"
import Card from "./Card"

function Cards() {
	return (
		<div className='container m-5 row'>
			{data.map(item => <Card key={item.id} data={item} />)}
		</div>
	);
}

export default Cards;