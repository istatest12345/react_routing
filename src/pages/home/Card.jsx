import React from 'react';

function Card({ data }) {
	const { company_name, short_desc, tags, img } = data
	return (
		<div className="card mb-3" style={{maxWidth: 540}}>
			<div className="row g-0">
				<div className="col-md-4">
					<img src={img} className="img-fluid rounded-start" alt="..." />
				</div>
				<div className="col-md-8">
					<div className="card-body">
						<h5 className="card-title">{company_name}</h5>
						<p className="card-text">{short_desc}</p>
						<p className="card-text"><small className="text-muted">{tags}</small></p>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Card;