import React from 'react';
import Navigation from "../components/Navigation"
import Cards from './Cards';

function Index() {
	return (
		<div>
			<Navigation />
			<div style={{backgroundColor: "lightcoral"}} className="d-flex justify-content-center flex-column m-5 p-5">
				<h2>Search</h2>
				<div className="container-fluid">
					<form className="d-flex">
						<input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
						<button className="btn btn-outline-success" type="button">Search</button>
					</form>
				</div>
			</div>
			<Cards />
		</div>
	);
}

export default Index;