import { useState } from 'react'
import './App.css'
import Index from './pages/home/Index'

function App() {
	return (
		<div className="App">
			<Index />
		</div>
	)
}

export default App
